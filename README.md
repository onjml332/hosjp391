#第一财经JJ租赁号服务公司tzt
#### 介绍
JJ租赁号服务公司【溦:844825】，JJ租赁号服务公司【溦:844825】/>　　　　　　上午：两种质地的硬物　　今天早早下了班，上街去转。张飞庙前幻灯般走动着黄包车与游客，甚至还有古装人赶着的马拉车。其实古街狭窄，当人流一涌两边的平房风景就退居其次了。行人稀少的时候，我倒是欣赏过很多次，那种清幽之气，古拙之姿，给人一种时光倒流的闪念。游人与居民，不知哪一方会发生感慨道：生活原来也可以如此悠闲呢！　　这感觉其实来自我的内心。那些傲放的红梅花，那样地如火如荼，又那样地闲情逸致。山茶花到处开放。我一闲下来就会想起它们，心里居然会有隐隐约约的模糊的被挠拨的乱与痛。　　不觉已到家门口了。在包里摸索钥匙，拿出几本刚借到的杂志。本想拥有翻闲书的快意，却不防生出一个突兀的念头：鲁迅说过，人类前行的历史正如煤的形成，当时用了大量的木材，结果却只是一小块；写作与煤的形成，何等相似！　　一走进家门，这些新鲜漂亮的书，如我的俘虏似的，再也不能激起我征服它们的欲望了。我都不想看它们一眼，完全没有欣赏的胃口。我的注意力只集中到一个中心事物上面：太阳！　　——多好的太阳啊，今天是打牌喝茶的天道！同事的话犹在心头回响。　　同事多是说说而已，我却已经受到了极大的诱惑，一直在想：到哪里去消磨这个半天呢？我躺在沙发上，还没停止盘算去哪里，带哪些书。　　门外的鸟儿欢叫着，在一片楼群里显得有些不真实。有人一声高一声低地吆喝：有废书废报纸卖！邻屋的厨房里动静越来越大，可以听见刀与菜，锅与油------宿舍变成了一个大蒸笼。我立即起身，去野地。　　在野外我接到一个陌生的电话。一个陌生的声音在我的视线之外响起，情绪饱满，仿佛林俊杰唱《江南》的那种调子。我无法形容一种发自嗓间的音乐，它来自刀郎或王菲，田震或谭咏麟，它们色彩鲜明，个性独具。可那是真切的成熟的男中音，磁性的，带着抒情意味（有点故作深情）的倾诉------让我的听觉一时感到迷惑。　　——我在读你的文章。窗外很冷，你却给我带来暖意。　　——我可以知道你是谁吗？　　——你不认识我，我是你的读者。　　　　　　　　　　　　下午：风景的漩涡　　太阳始终没有露面。我已经来到山中。　　一阵风一阵风，中间稍有间歇，好像两山之间的气流，用均匀的频率振动我的衣服和头发。　　湖边，我是唯一的茶客，冷静，独立而自足，像那些暗含春意的柳丝。一阵风过，我感觉刚刚骑车出来的一身热气已经消失，我迷惘地抬起头来。柳丝间的枯叶如小虫子，吱吱叫着落入湖岸的草丛。这是个人工湖。询问走过湖边的男子，他说这湖有几部分，方园一里余，十余亩，里面什么鱼都有，水可以流通鱼不可以，并且还有一分为四的专养鱼苗的塘。　　我不断地往杯里加水。茶一沉淀我就喝水，以抵心底特别是头顶的凉气。湖里的光影不断地变化，近处倒映着褐色和淡绿，那是已长出花苞的柳和对面山坡上黄绿相间的竹；远处是亮亮的波光，明亮的鱼肚白。当一湖水安静时，那水面自有一种使人心熨贴的纹路的，好像皮肤的纵横交错的神秘的线条。这些梯田或皮肤似的线条，灵动而奇幻。　　这是正月的下旬天气——我仍然无法忘记当下的天气，太阳今天是不会出来的——冷风是如此有穿透力，它仿佛正在浸入我的骨髓，我在变凉，恍若被抛在岸边呼吸艰难的鱼------而东风如此浩荡！　　春风，我固执地将染了梅花红、柳丝绿的风，叫做春风。我有时在风中行走，像一只整天冬眠的猫，忽然想起了她那床小河。我在风中疾驰，冰凉的风使我感到难以言表的快意。又一场春雪，你给予我们如此丰富的享受，谁在意那些尖刻的批评呢？又一阵风来，我被轻轻地吹拂，我的脸，我的湖水，都浸在茫茫然的快乐之中。我长久地凝视这一方湖水，我看到它们瞬息万变的心境，看到它们生动活泼的存在。来一阵风时，那最远处的岸边，跳荡着动感的高亢的音符，就像电脑中那些激越的音乐花，无声地叠映在我眼前，让我震撼：一湖水，也可以掀起如此灿烂的激情，只要有风轻轻吹------　　　　　　晚上：爱情何须幻想　　像一个理想主义者爱上他的梦，我在额外的闲适生活中放纵自己的幻想，我钟情那些幽独的文字，为之上瘾欢欣又暗自嗟呀。　　没有人能通过我斩钉截铁或唠唠叨叨的话语看出我内心的优柔。我没有理由为季节和人事而感伤，除了春雷，我的心不能够被震动。但是一树艳丽的红梅，一坡富丽的茶花，一个问候的电话------温暖的感伤如春水荡漾，使我的心常常湿润着。　　有时候孤独是特别适合我的。孤独给人灵感，给人力量。就像那些在北极爬冰卧雪的人，体验了孤独就不会再害怕喧嚣。我带了几本书坐在湖边，可是不想读什么长篇大论，我只读着几首小诗，然后思绪就飘渺到不知归途。我现在不是一个需要指引的人，我的心愿强烈地支配着我的行动，如果我不达成这个目标我将一直受到灵魂的拷打。这个秘密使命使我食不甘味，夜不能寐。我现在知道，不是我消化不良，而是我将生产。我有强烈的表达的需要，可是我不知道如何开口说第一句话。胎生在我生命里的哪一场爱情，是笼罩我一生的阴影呢？　　越接近开口说话，我越有一种宿命感。对爱情没有什么人特别有免疫力。有的人只要一出现，就摧毁了你的生命和情感------总是如此。这就是我对中财上面那个《爱要怎么说出口》的故事所下的断语，是对我的感情生活所作的归结。寂寞沙洲啊，幸亏你没有说出来，尽管你爱他十年，尽管你为他而离婚，但我想你爱的这个他即使离婚也肯定不是为你，只可能是为他的那个初恋，他可能会去拯救他最初和最纯的爱恋，而你，始终只是他的妹妹。你说是不是呢？　　为了你的这份痴爱，我写下一段不太动听的歌，请听我唱：　　爱情的水深火热，你如何抵挡　　不如早些逃脱，沐浴你事业的荣光　　爱情是海，爱如潮水，惊涛拍岸　　爱情是火，焚心以火，心如死灰　　爱情酿造的蜜酒，渗透你如同毒药　　生死肉骨，爱情如何避让　　但你可以不再幻想　　柳丝在水上漫舞，爱的是自己的模样　　湖水虽然平静，也喜丝丝缕缕的撩拨　　当垂柳枯枝如发一冬孤寂　　波光潋滟的湖水并不稍离　　彼此相守相互安慰也是爱　　世俗的爱情与人间烟火　　你何以清高到可以无视　　我可爱的仙女　　　　　　　　　　　　　　　　（2006-2-22）
　　纵然吃了那么多的烧饵块，却仍然想念家乡那只一点辣椒与酱油便生出极美滋味的东西，也记得，那在炉火旁的等候，那一个个在铁架上软和微黄并鼓胀起来的饵块，是怎样快乐了我的童年。

#### 参与贡献
Fork 本仓库
新建 Feat_xxx 分支
提交代码
新建 Pull Request
#### 特技
使用 Readme_XXX.md 来支持不同的语言，例如 Readme_en.md, Readme_zh.md
Gitee 官方博客 blog.gitee.com
你可以 https://gitee.com/explore 这个地址来了解 Gitee 上的优秀开源项目
GVP 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
Gitee 官方提供的使用手册 https://gitee.com/help
Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 https://gitee.com/gitee-stars/